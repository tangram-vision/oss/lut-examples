#!/usr/bin/env python
# coding: utf-8
"""\
This script loads all files necessary to experiment with Tangram's plane detection LUTs.
Double-click on a pixel in the image to see its corresponding position on the ground plane.

Usage: plane_lut.py <training_image> lut.exr lut_mask.png

Note: Users may need OpenEXR compatibility. If this format is not recognized by your host machine,
install it via apt.

```shell
sudo apt install openexr
```
"""

import argparse
import cv2
import os

os.environ["OPENCV_IO_ENABLE_OPENEXR"] = "1"


def print_distance(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        lut = param[0]
        lut_mask = param[1]
        plane_pose_x, plane_pose_y, plane_pose_z = lut[(y, x)]
        mask_x, mask_y, mask_z = lut_mask[(y, x)]
        if mask_x == 0 and mask_y == 0 and mask_z == 0:
            plane_pose_x, plane_pose_y = "N/A", "N/A"
        if mask_x == 255:
            plane_pose_x = -plane_pose_x
        if mask_y == 255:
            plane_pose_y = -plane_pose_y
        if mask_z == 255:
            plane_pose_z = -plane_pose_z
        print("Plane coordinates at click: ", plane_pose_x, ", ", plane_pose_y)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_image")
    parser.add_argument("lut")
    parser.add_argument("lut_mask")
    args = parser.parse_args()

    # Read the image
    img = cv2.imread(args.input_image)

    # This LUT is a RGB32F image, with each RGB pixel representing a 3D coordinate on the projected plane
    # from that pixel.
    # [X: red, Y: green, Z: blue]
    #
    # Note: The origin of this coordinate system is wherever the origin of the target was placed on
    # the ground. This could be changed to the camera origin if necessary.
    lut = cv2.imread(args.lut, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
    # OpenCV Reads images in natively as BGR. Convert here to avoid mixups
    lut = cv2.cvtColor(lut, cv2.COLOR_BGR2RGB)

    # The lookup table of signs and validity of the 3D coordinates per pixel. The sign/validity is
    # represented by an RGB8 value:
    # [X: red, Y: green, Z: blue]
    # - channel == 0: not valid
    # - channel == u8::MAX/2: positive sign
    # - channel == u8: negative sign
    lut_mask = cv2.imread(args.lut_mask, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
    # OpenCV Reads images in natively as BGR. Convert here to avoid mixups
    lut_mask = cv2.cvtColor(lut_mask, cv2.COLOR_BGR2RGB)

    # Resize all of our images to fit on the screen
    scale_percent = 30  # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)

    img_resize = cv2.resize(img, dim)
    lut_resize = cv2.resize(lut, dim)
    lut_mask_resize = cv2.resize(lut_mask, dim)

    window_name = "image"
    cv2.namedWindow(window_name)
    param = [lut_resize, lut_mask_resize]
    cv2.setMouseCallback(window_name, print_distance, param)

    while True:
        cv2.imshow(window_name, img_resize)
        k = cv2.waitKey(20) & 0xFF
        if k == 27:
            cv2.destroyAllWindows()
            break


if __name__ == "__main__":
    main()
