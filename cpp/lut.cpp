#include "nlohmann/json.hpp"
#include <fstream>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

nlohmann::json readJsonFile(std::ifstream &f) {
  nlohmann::json json_data;
  f >> json_data;
  return json_data;
}

int main(int argc, char *argv[]) {
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " <path_to_image> <path_to_json_file>"
              << std::endl;
    return EXIT_FAILURE;
  }
  std::string image_filename(argv[1]);
  std::string json_filename(argv[2]);

  std::ifstream json_f(json_filename);
  if (!json_f) {
    std::cerr << "Error: Could not open the file: " << json_filename
              << std::endl;
    exit(EXIT_FAILURE);
  }
  nlohmann::json lut_json = readJsonFile(json_f);

  cv::Mat image = cv::imread(image_filename, cv::IMREAD_COLOR);
  if (image.empty()) {
    std::cerr << "Error: Could not open or find the image: " << image_filename
              << std::endl;
    exit(EXIT_FAILURE);
  }

  std::vector<std::vector<float>> lut = lut_json["lut"];
  cv::Mat map_x(image.size(), CV_32F);
  cv::Mat map_y(image.size(), CV_32F);
  for (size_t i = 0; i < lut.size(); ++i) {
    const auto &pix = lut[i];
    map_x.at<float>(i) = pix[0];
    map_y.at<float>(i) = pix[1];
  }
  cv::Mat image_corrected;
  cv::remap(image, image_corrected, map_x, map_y, cv::INTER_LINEAR);

  cv::Mat images;
  cv::hconcat(image, image_corrected, images);

  std::cout << "Camera Intrinsics: "
            << lut_json["camera"]["intrinsics"]["projection"] << std::endl;

  constexpr double scale_factor = 0.25;
  cv::Mat display_images;
  const int height = static_cast<int>(images.size[0] * scale_factor);
  const int width = static_cast<int>(images.size[1] * scale_factor);
  cv::resize(images, display_images, {width, height});
  cv::imshow("Correct Image", display_images);
  cv::waitKey(0);

  return 0;
}
