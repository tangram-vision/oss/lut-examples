# Image Look Up Tables

This repository holds examples of different image manipulations using per-pixel look up tables, or
LUTs.

## Image Rectification and Correction Look Up Tables

Tangram's image correction look up tables (LUT) are compatible with with [opencv's
remap](https://docs.opencv.org/4.7.0/d1/da0/tutorial_remap.html) function. This repo provides
examples of how to load in Tangram's LUTs and use them to correct an image.

See the folder corresponding to your language of choice for how to use our LUTs
in your program.

## Ground Plane Projection Look Up Tables

Tangram's plane projection LUTs can be imported into OpenCV as a three-channel f32 image. These LUTs
are stored as an OpenEXR file (.exr) for easy retrieval. Each .exr must be accompanied by its plane
projection LUT mask, stored as a .png.

See `/python/plane_lut.py` for an example of loading and retrieving ground plane values given a
training image.

# License

This project and associated code is provided under the MIT License. See [LICENSE.md](LICENSE.md) for
the full text.
